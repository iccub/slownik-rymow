package pl.bucci.dev.slownik.rymow.search_form

/**
 * slownik-rymow
 *
 * Created by michalbuczek on 19.12.2016.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

class SearchParameters(val word: String, val rhymeLength: Int, val rhymePrecisionEnum: RhymePrecisionEnum, val sortMethodEnum: SortMethodEnum) {
    companion object {
        const val DEFAULT_RHYME_LENGTH = 3
    }
}
