package pl.bucci.dev.slownik.rymow.main

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import pl.bucci.dev.slownik.rymow.BuildConfig
import pl.bucci.dev.slownik.rymow.R
import pl.bucci.dev.slownik.rymow.dialogs.DialogHelper
import pl.bucci.dev.slownik.rymow.search_form.RhymeFinderManager
import pl.bucci.dev.slownik.rymow.search_form.RhymePrecisionEnum
import pl.bucci.dev.slownik.rymow.search_form.SearchParameters
import pl.bucci.dev.slownik.rymow.search_form.SortMethodEnum
import pl.bucci.dev.slownik.rymow.word_definition.WordDefinitionHelper
import pl.bucci.dev.slownik.rymow.word_list.WordAdapter
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    lateinit private var wordList: ArrayList<String>
    lateinit private var wordListAdapter: WordAdapter
    lateinit private var progressDialog: ProgressDialog
    lateinit private var dialogHelper: DialogHelper
    lateinit private var wordDefinitionHelper: WordDefinitionHelper
    lateinit private var appHelper: AppHelper
    lateinit private var adHelper: AdHelper
    lateinit private var rhymeFormHelper: RhymeFormHelper

    private val sortMethod: SortMethodEnum
        get() = if(radioSortNormal.isChecked) SortMethodEnum.ALPHABETICAL else SortMethodEnum.RANDOM

    private val rhymesPrecision: RhymePrecisionEnum
        get() = if(radioAccurateRhymes.isChecked) RhymePrecisionEnum.ACCURATE else RhymePrecisionEnum.INACCURATE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setDefaultValues()
        initViewListeners()

        wordList = ArrayList()
        dialogHelper = DialogHelper(supportFragmentManager)
        wordListAdapter = WordAdapter(this, wordList)
        foundWordsList.adapter = wordListAdapter
        wordDefinitionHelper = WordDefinitionHelper(this, dialogHelper)
        appHelper = AppHelper(this)
        rhymeFormHelper = RhymeFormHelper()

        if (!BuildConfig.PRO_VERSION) {
            adHelper = AdHelper()
            initAdView()
        }
    }

    override fun onResume() {
        super.onResume()
        adView.resume()
    }

    override fun onPause() {
        super.onPause()
        adView.pause()
    }

    override fun onDestroy() {
        adView.destroy()
        super.onDestroy()
    }

    @SuppressLint("SetTextI18n")
    private fun setDefaultValues() {
        rhymeLength.text = SearchParameters.DEFAULT_RHYME_LENGTH.toString()
    }

    private fun initViewListeners() {
        wordToSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) searchRhymeButton.performClick()
            false
        }

        foundWordsList.onItemClickListener = AdapterView.OnItemClickListener { adapterView, _, i, _ ->
            if (appHelper.isOnline) {
                val clickedWord = adapterView.getItemAtPosition(i).toString()
                wordDefinitionHelper.showWordDefinition(clickedWord)
            } else {
                showNetworkErrorDialog()
            }
        }

        rhymeLengthSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            @SuppressLint("SetTextI18n")
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                // SeekBar value always starts from 0, so we need to adjust for that
                val value = progress + SearchParameters.DEFAULT_RHYME_LENGTH
                rhymeLength.text = value.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
    }

    private fun showNetworkErrorDialog() {
        dialogHelper.showDialog(getString(R.string.network_error_title), getString(R.string.network_error_msg))
    }

    private fun initAdView() {
        adView.loadAd(adHelper.adRequest)
    }

    fun searchRhymeButtonClicked(view: View) {
        if (DEBUG) Log.d(TAG, "searchRhymeButtonClicked")

        if (searchedWord == "") {
            appHelper.showKeyboard(wordToSearch)
            return
        }

        val parameters = SearchParameters(searchedWord.toLowerCase(),
                rhymeFormHelper.getRhymeLength(rhymeLength.text.toString(), searchedWord),
                rhymesPrecision,
                sortMethod)

        progressDialog = ProgressDialog.show(this@MainActivity, "",
                getString(R.string.search_rhyme_progress_text))

        async(UI) {
            val foundRhymes: Deferred<ArrayList<String>> = bg {
                findRhymes(parameters)
            }

            showFoundRhymes(foundRhymes.await())
        }

        appHelper.hideKeyboard(wordToSearch)
    }

    private fun showFoundRhymes(data: ArrayList<String>) {
        progressDialog.dismiss()
        if (data.isEmpty()) {
            dialogHelper.showDialog(getString(R.string.no_rhymes_found_dialog_text) + wordToSearch.text, "")
        }

        wordList = data
        fillWordList()
    }

    private fun findRhymes(parameters: SearchParameters): ArrayList<String> {
        val rhymeFinderManager = RhymeFinderManager(applicationContext)

        return rhymeFinderManager.findRhymesWithParameters(parameters)
    }

    private val searchedWord: String
        get() = wordToSearch.text.toString()

    private fun fillWordList() {
        wordListAdapter.clear()
        wordListAdapter.addAll(wordList)
        wordListAdapter.notifyDataSetChanged()
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        if (BuildConfig.PRO_VERSION) menu.removeItem(R.id.buy_pro)
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.about_app -> showAboutAppDialog()
            R.id.rate_app -> showAppPage(appHelper.appUriForBuild)
            R.id.buy_pro -> showAppPage(appHelper.premiumAppUri)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showAboutAppDialog() {
        try {
            dialogHelper.showDialog(getString(R.string.app_name) + " " + appHelper.versionName,
                    getString(R.string.about_app))
        } catch (e: PackageManager.NameNotFoundException) {
            Log.e(TAG, e.message)
        }

    }

    private fun showAppPage(uri: Uri) {
        try {
            startActivity(Intent(Intent.ACTION_VIEW, uri))
        } catch (e: ActivityNotFoundException) {
            dialogHelper.showDialog(
                    getString(R.string.gplay_connect_error_title), getString(R.string.gplay_connect_error_msg))
        }
    }

    companion object {
        private val TAG = "BCC|MainActivity"
        private val DEBUG = false
    }
}