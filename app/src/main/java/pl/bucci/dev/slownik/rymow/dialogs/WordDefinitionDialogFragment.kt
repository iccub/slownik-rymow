package pl.bucci.dev.slownik.rymow.dialogs


import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import pl.bucci.dev.slownik.rymow.R

class WordDefinitionDialogFragment : AppDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(msg)
                .setTitle(title)
                .setPositiveButton(R.string.ok_button, null)
                .setNeutralButton(R.string.copy_word_button) { _, _ ->
                    val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                    val clip = ClipData.newPlainText(getString(R.string.clipboard_copied_text_label), title)
                    clipboard.primaryClip = clip
                }

        return builder.create()
    }
}
