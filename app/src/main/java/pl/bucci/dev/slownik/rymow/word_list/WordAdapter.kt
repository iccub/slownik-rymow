package pl.bucci.dev.slownik.rymow.word_list

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter

import java.util.ArrayList

import pl.bucci.dev.slownik.rymow.R

/**
 * slownik-rymow
 *
 *
 * Created by michalbuczek on 16.08.2017.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

class WordAdapter(context: Context, wordList: ArrayList<String>?) : ArrayAdapter<String>(context, 0, wordList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        val word = getItem(position)
        val wordViewHolder: WordViewHolder

        return if (view == null) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_word, parent, false)
            wordViewHolder = WordViewHolder(view)
            view.tag = wordViewHolder
            wordViewHolder.word.text = word

            view
        } else {
            wordViewHolder = view.tag as WordViewHolder
            wordViewHolder.word.text = word

            view
        }
    }
}
