package pl.bucci.dev.slownik.rymow.dialogs

import android.os.Bundle
import android.support.v4.app.FragmentManager

/**
 * slownik-rymow
 *
 *
 * Created by michalbuczek on 16.08.2017.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

class DialogHelper(private val fragmentManager: FragmentManager) {

    fun showWordDefinitionDialog(title: String, msg: String) {
        presentDialog(title, msg, DialogType.WORD_DEFINITION)
    }

    fun showDialog(title: String, msg: String) {
        presentDialog(title, msg, DialogType.SIMPLE)
    }

    private fun presentDialog(title: String, msg: String, type: DialogType) {
        val fragment = if (type == DialogType.WORD_DEFINITION) WordDefinitionDialogFragment() else SimpleDialogFragment()

        val bundle = Bundle()
        bundle.putString(AppDialogFragment.APP_DIALOG_ARG_MESSAGE, msg)
        bundle.putString(AppDialogFragment.APP_DIALOG_ARG_TITLE, title)

        fragment.arguments = bundle
        fragment.show(fragmentManager, AppDialogFragment.APP_DIALOG_TAG)
    }
}
