package pl.bucci.dev.slownik.rymow.dialogs

/**
 * slownik-rymow
 *
 *
 * Created by michalbuczek on 16.08.2017.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

enum class DialogType {
    WORD_DEFINITION, SIMPLE
}
