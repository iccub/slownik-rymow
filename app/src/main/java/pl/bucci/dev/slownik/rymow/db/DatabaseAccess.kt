package pl.bucci.dev.slownik.rymow.db

/*
  slownik-rymow

  Created by michalbuczek on 19.12.2016.
  Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.util.ArrayList

import pl.bucci.dev.slownik.rymow.search_form.RhymePrecisionEnum
import pl.bucci.dev.slownik.rymow.search_form.SearchParameters
import pl.bucci.dev.slownik.rymow.search_form.SortMethodEnum


class DatabaseAccess private constructor(context: Context) {

    private val openHelper: SQLiteOpenHelper
    private var database: SQLiteDatabase? = null

    init {
        this.openHelper = DBConnectionHelper(context)
    }

    fun open() {
        this.database = openHelper.readableDatabase
    }

    fun close() {
        this.database?.close()
    }

    @SuppressLint("Recycle")
    fun findRhymesWithParameters(parameters: SearchParameters): ArrayList<String> {
        val cursor = database?.rawQuery(prepareQuery(parameters), null) ?: return ArrayList()

        return getWordsReversed(cursor)
    }

    private fun prepareQuery(params: SearchParameters): String {
        val rhymeLength = params.rhymeLength

        val rhyme = getRhymePartReversed(params.word, rhymeLength)
        val algorithmHelper = AlgorithmQueryHelper(rhyme)

        val query = StringBuilder(
                "SELECT * FROM $tableName WHERE $columnName MATCH '$rhyme* ")

        val uRuleQuery = algorithmHelper.addURule()
        uRuleQuery?.let { query.append(algorithmHelper.addURule()) }

        if (params.rhymePrecisionEnum == RhymePrecisionEnum.INACCURATE) {
            for (nonPreciseQuery in algorithmHelper.nonPreciseQueries) {
                query.append(nonPreciseQuery)
            }
        }

        //closing match statement
        query.append("' ")

        if (params.sortMethodEnum == SortMethodEnum.RANDOM) query.append("ORDER BY RANDOM() ")

        // 200 words is enough results to look through
        query.append("limit ${Companion.RHYME_RESULTS_LIMIT};")

        return query.toString()
    }

    private fun getWordsReversed(cursor: Cursor): ArrayList<String> {
        val foundRhymes = ArrayList<String>()

        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            val foundRhymeReversedToNormal = StringBuilder(cursor.getString(0)).reverse().toString()

            foundRhymes.add(foundRhymeReversedToNormal)
            cursor.moveToNext()
        }
        cursor.close()

        return foundRhymes
    }

    // Full word is not needed, we search only the part selected as a rhyme
    // Also the rhyme needs to be reversed, as user input is in regular order and db words are reversed
    private fun getRhymePartReversed(word: String, rhymeLength: Int): String {
        val trimAmount = if (rhymeLength > word.length) word.length else rhymeLength

        val sb = StringBuilder(word.substring(word.length - trimAmount, word.length))
        return sb.reverse().toString()
    }

    companion object {
        private var instance: DatabaseAccess? = null

        private val tableName = "word_list"
        private val columnName = "word"

        fun getInstance(context: Context): DatabaseAccess {
            if (instance == null) {
                instance = DatabaseAccess(context)
            }
            return instance as DatabaseAccess
        }

        private val RHYME_RESULTS_LIMIT = 200
    }
}