package pl.bucci.dev.slownik.rymow.word_definition

/*
  slownik-rymow

  Created by michalbuczek on 19.12.2016.
  Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

import android.content.Context
import android.util.Log
import org.jsoup.Jsoup
import pl.bucci.dev.slownik.rymow.R

internal class SjpParser(private val context: Context, private val htmlResponse: ByteArray) {

    // On sjp.pl website, word definition comes in two types:
    // - single description, when there is only one word meaning
    // - multiple descriptions as ordered list, for words with many meanings(like sjp.pl/zamek)
    // To make word definition popup text consistent, all ordered lists are replaced with dash, and to single
    // descriptions dash is added at the beginning.
    val wordDefinition: String
        get() {
            val response = String(htmlResponse)

            val doc = Jsoup.parse(response)
            val paragraphs = doc.select(PARSER_SELECTOR_REGEX)

            if (DEBUG) Log.i(TAG, "word def paragraphs found: " + paragraphs.size)

            val sb = StringBuilder()
            for (p in paragraphs) {
                val text = p.text()
                val multipleMeanings = text.split(DIGIT_DOT_REGEX.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                // Multiple descriptions
                // first element is a space, skipping it
                if (multipleMeanings.size > 1) {
                    for (i in 1 until multipleMeanings.size) {
                        sb.append("- ").append(multipleMeanings[i].trim { it <= ' ' }).append("\n")
                    }
                } else {
                    // Single description
                    sb.append("- ").append(p.text()).append("\n")
                }
            }

            return if (paragraphs.isEmpty()) context.getString(R.string.no_word_definition_found) else sb.toString()
        }

    companion object {
        private val TAG = "BCC|SjpParser"
        private val DEBUG = false

        // This selector may change from time to time, which results in not getting word definition
        // from sjp.pl properly
        private val PARSER_SELECTOR_REGEX = "p[style*=font-size][style*=max-width]"
        private val DIGIT_DOT_REGEX = "[0-9]\\."
    }
}
