package pl.bucci.dev.slownik.rymow.word_list

import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.adapter_word.view.*


/**
 * slownik-rymow
 *
 *
 * Created by michalbuczek on 16.08.2017.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

class WordViewHolder(baseView: View) {
    var word: TextView = baseView.wordTextView
}
