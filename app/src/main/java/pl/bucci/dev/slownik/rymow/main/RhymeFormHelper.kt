package pl.bucci.dev.slownik.rymow.main

/**
 * slownik-rymow
 *
 *
 * Created by michalbuczek on 18.08.2017.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

internal class RhymeFormHelper {

    fun getRhymeLength(rhymeLength: String, searchedWord: String): Int {
        val rhymeLengthInt = Integer.parseInt(rhymeLength)

        return if (searchedWord.length < rhymeLengthInt) searchedWord.length else rhymeLengthInt
    }
}
