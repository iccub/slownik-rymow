package pl.bucci.dev.slownik.rymow.dialogs

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AlertDialog

import pl.bucci.dev.slownik.rymow.R

/**
 * slownik-rymow
 *
 *
 * Created by michalbuczek on 16.08.2017.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

class SimpleDialogFragment : AppDialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(msg)
                .setTitle(title)
                .setPositiveButton(R.string.ok_button, null)

        return builder.create()
    }
}
