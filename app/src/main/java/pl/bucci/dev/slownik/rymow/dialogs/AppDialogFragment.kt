package pl.bucci.dev.slownik.rymow.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment

/**
 * slownik-rymow
 *
 *
 * Created by michalbuczek on 16.08.2017.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

open class AppDialogFragment : DialogFragment() {

    internal var msg: String? = null
    internal var title: String? = null

    override fun setArguments(args: Bundle) {
        super.setArguments(args)

        msg = args.getString(APP_DIALOG_ARG_MESSAGE)
        title = args.getString(APP_DIALOG_ARG_TITLE)
    }

    companion object {
        val APP_DIALOG_TAG = "APP_DIALOG_TAG"
        val APP_DIALOG_ARG_MESSAGE = "APP_DIALOG_ARG_MESSAGE"
        val APP_DIALOG_ARG_TITLE = "APP_DIALOG_ARG_TITLE"
    }
}
