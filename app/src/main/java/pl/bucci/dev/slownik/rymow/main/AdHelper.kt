package pl.bucci.dev.slownik.rymow.main

import com.google.android.gms.ads.AdRequest

/**
 * slownik-rymow
 *
 *
 * Created by michalbuczek on 16.08.2017.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

internal class AdHelper {

    val adRequest: AdRequest
        get() {

            return if (DEBUG) {
                AdRequest.Builder()
                        .addTestDevice("A9C2ED8BD36F029D529C0AAF5430291E")
                        .build()
            } else {
                AdRequest.Builder().build()
            }
        }

    companion object {
        private val DEBUG = false
    }
}
