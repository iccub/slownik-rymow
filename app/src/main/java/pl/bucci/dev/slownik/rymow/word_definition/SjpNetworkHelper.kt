package pl.bucci.dev.slownik.rymow.word_definition

import retrofit2.Retrofit

/**
 * slownik-rymow
 *
 *
 * Created by michalbuczek on 16.08.2017.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

internal class SjpNetworkHelper {

    val sjpService: SjpService
        get() {
            return Retrofit.Builder().baseUrl(SJP_URL).build().create(SjpService::class.java)
        }

    companion object {
        private val SJP_URL = "http://sjp.pl"
    }
}