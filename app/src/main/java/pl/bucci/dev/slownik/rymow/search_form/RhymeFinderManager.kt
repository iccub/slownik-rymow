package pl.bucci.dev.slownik.rymow.search_form

import android.content.Context
import pl.bucci.dev.slownik.rymow.db.DatabaseAccess
import java.util.*

/**
 * slownik-rymow
 *
 * Created by michalbuczek on 19.12.2016.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

class RhymeFinderManager(private val context: Context) {

    fun findRhymesWithParameters(parameters: SearchParameters): ArrayList<String> {
        val databaseAccess = DatabaseAccess.getInstance(context)
        databaseAccess.open()
        val foundRhymes = databaseAccess.findRhymesWithParameters(parameters)
        databaseAccess.close()

        return foundRhymes
    }
}
