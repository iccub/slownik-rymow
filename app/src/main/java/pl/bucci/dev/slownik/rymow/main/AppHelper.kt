package pl.bucci.dev.slownik.rymow.main


import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import pl.bucci.dev.slownik.rymow.BuildConfig

/**
 * slownik-rymow
 *
 *
 * Created by michalbuczek on 16.08.2017.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

internal class AppHelper(private val context: Context) {

    val appUriForBuild: Uri
        get() = if (BuildConfig.PRO_VERSION) premiumAppUri else regularAppUri


    val premiumAppUri: Uri
        get() = Uri.parse("market://details?id=" + context.packageName + ".pro")

    private val regularAppUri: Uri
        get() = Uri.parse("market://details?id=" + context.packageName)

    fun showKeyboard(view: View) {
        val imm = context.applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, 0)
    }

    fun hideKeyboard(view: View) {
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    val versionName: String
        @Throws(PackageManager.NameNotFoundException::class)
        get() = context.packageManager.getPackageInfo(context.packageName, 0).versionName

    val isOnline: Boolean
        get() {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            val netInfo = cm.activeNetworkInfo
            return netInfo.isConnectedOrConnecting
        }
}
