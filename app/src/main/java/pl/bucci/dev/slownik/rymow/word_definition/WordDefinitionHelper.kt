package pl.bucci.dev.slownik.rymow.word_definition

import android.content.Context
import android.util.Log
import okhttp3.ResponseBody
import pl.bucci.dev.slownik.rymow.R
import pl.bucci.dev.slownik.rymow.dialogs.DialogHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * slownik-rymow
 *
 *
 * Created by michalbuczek on 16.08.2017.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

class WordDefinitionHelper(private val context: Context, private val dialogHelper: DialogHelper) {

    fun showWordDefinition(word: String) {
        val sjpHelper = SjpNetworkHelper()
        val sjpService = sjpHelper.sjpService

        val wordsCall: Call<ResponseBody>
        wordsCall = sjpService.wordDefinition(word)

        wordsCall.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    val parser = SjpParser(context, response.body()!!.bytes())
                    dialogHelper.showWordDefinitionDialog(word, parser.wordDefinition)
                } catch (e: Exception) {
                    Log.e(TAG, "Exception: " + e.message)
                    showSjpConnectionError(word)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                showSjpConnectionError(word)
            }
        })
    }

    private fun showSjpConnectionError(word: String) {
        dialogHelper.showDialog(word, context.getString(R.string.sjp_connect_error))
    }

    companion object {
        private val TAG = "BCC|WordDefinitionHelp"
    }
}
