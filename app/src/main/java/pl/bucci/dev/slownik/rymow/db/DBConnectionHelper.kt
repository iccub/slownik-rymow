package pl.bucci.dev.slownik.rymow.db

/*
  slownik-rymow

  Created by michalbuczek on 19.12.2016.
 */

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper
import android.content.Context

internal class DBConnectionHelper(context: Context) : SQLiteAssetHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        private val DATABASE_NAME = "db-fts-reversed.db"
        private val DATABASE_VERSION = 1
    }
}
