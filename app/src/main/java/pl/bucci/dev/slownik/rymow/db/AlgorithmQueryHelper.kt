package pl.bucci.dev.slownik.rymow.db

import java.util.regex.Pattern

/**
 * slownik-rymow
 *
 *
 * Created by michalbuczek on 16.08.2017.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

internal class AlgorithmQueryHelper(private val rhyme: String) {

    private val reversedConsonantsArray = arrayOf("b", "c", "ć", "d", "f", "g", "h", "j", "k", "l", "ł", "m", "n", "ń",
            "p", "r", "s", "ś", "t", "w", "z", "ź", "ź", "zc", "zd", "źd", "żd", "zs", "hc", "zr")

    //Letters 'u' and 'ó' sound the same phonetically. Checking both possibilities.
    fun addURule(): String? {
        if (!rhyme.contains("u") && !rhyme.contains("ó")) return null

        val wordWithUSwapped = if (rhyme.contains("u")) rhyme.replace("u", "ó") else rhyme.replace("ó", "u")
        return getMatchStatement(wordWithUSwapped)
    }

    val nonPreciseQueries: ArrayList<String>
        get() {
            val queriesList = ArrayList<String>()

            queriesList.addAll(consonantAtRhymeEndQueries)
            queriesList.addAll(replaceFirstConsonantQueries())

            return queriesList
        }

    // Adds consonants at the of of rhyme
    // For example rhyme 'mek' adds consonant at the end and result can be 'pumeks'
    private val consonantAtRhymeEndQueries: ArrayList<String>
        get() {
            return reversedConsonantsArray.mapTo(ArrayList()) { getMatchStatement(it + rhyme) }
        }

    //Looking for last consonant(first in reverse) in the word and swap it with other consonants
    // For example with rhyme 'mek' replace letter 'm' with other consonants, resulting in 'pasek' as a found word
    private fun replaceFirstConsonantQueries(): ArrayList<String> {
        val reversedConsonantsPattern = Pattern.compile("(zc|zd|źd|żd|zs|hc|zr|[bcćdfghjklłmnńprsśtwzżź])")
        val matcher = reversedConsonantsPattern.matcher(rhyme)

        if (!matcher.find()) return ArrayList()

        val firstMatch = matcher.group()

        val firstIndex = rhyme.indexOf(firstMatch)
        val consonantLength = if (firstMatch.length > 1) 2 else 1

        return reversedConsonantsArray.mapTo(ArrayList()) {
            val newWord = StringBuilder(rhyme)
            newWord.replace(firstIndex, firstIndex + consonantLength, it)

            getMatchStatement(newWord.toString())
        }
    }

    private fun getMatchStatement(wordToCompare: String): String {
        return "OR $wordToCompare* "
    }
}
