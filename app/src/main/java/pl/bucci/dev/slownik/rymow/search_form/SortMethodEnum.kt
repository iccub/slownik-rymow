package pl.bucci.dev.slownik.rymow.search_form

/**
 * slownik-rymow
 *
 * Created by michalbuczek on 19.12.2016.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

enum class SortMethodEnum {
    ALPHABETICAL, RANDOM
}