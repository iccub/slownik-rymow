package pl.bucci.dev.slownik.rymow.word_definition

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * slownik-rymow
 *
 *
 * Created by michalbuczek on 16.08.2017.
 * Copyright (c) 2016 Michał Buczek. All rights reserved.
 */

internal interface SjpService {
    @GET("{word}")
    fun wordDefinition(@Path("word") word: String): Call<ResponseBody>
}
